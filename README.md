BBuddy
===

## Installation
Please install the following tools for this project. The latest version should be fine unless specific version is listed.

- Git
- JDK 1.8
- Gradle 2.13
- MySQL
  - database: bbuddydev
  - username: nerd
  - password: dbs3cr3t
- Tomcat 8.5.x
